## Mage Robes for OpenMW-Lua

#### 1.8

* Added an exception for a certain Tamriel Rebuilt NPC in advance of the upcoming update

[Download Link](https://gitlab.com/modding-openmw/mage-robes-for-openmw-lua/-/packages/33086258)

#### 1.7

* Fixed the mod not working at all due to changes made to the OpenMW-Lua API

[Download Link](https://gitlab.com/modding-openmw/mage-robes-for-openmw-lua/-/packages/30483498)

#### 1.6

* Added [Astrologian's Guild](https://www.nexusmods.com/morrowind/mods/51216) NPCs to the blacklist

[Download Link](https://gitlab.com/modding-openmw/mage-robes-for-openmw-lua/-/packages/25205777)

#### 1.5

* Fixed issues with Skyrim: Home of the Nords
* Added translation files

[Download Link](https://gitlab.com/modding-openmw/mage-robes-for-openmw-lua/-/packages/22287617)

#### 1.4

* Fixed interface typo

[Download Link](https://gitlab.com/modding-openmw/mage-robes-for-openmw-lua/-/packages/21556373)

#### 1.3

* Code cleanup and optimization
* Added an interface for adding mods and NPCs to the internal blacklists

[Download Link](https://gitlab.com/modding-openmw/mage-robes-for-openmw-lua/-/packages/21556312)

#### 1.2

* Better filtering of which NPCs should get robes

[Download Link](https://gitlab.com/modding-openmw/mage-robes-for-openmw-lua/-/packages/21442326)

#### 1.1

* Better removal of old robes

[Download Link](https://gitlab.com/modding-openmw/mage-robes-for-openmw-lua/-/packages/21441362)

#### 1.0

* Initial release

[Download Link](https://gitlab.com/modding-openmw/mage-robes-for-openmw-lua/-/packages/21433149)
