# Mage Robes for OpenMW-Lua

Dynamically dresses NPCs with [Mage Robes by Melchior Dahrk](https://www.nexusmods.com/morrowind/mods/45739).

**Requires OpenMW 0.49 or newer and [Mage Robes by Melchior Dahrk](https://www.nexusmods.com/morrowind/mods/45739)!**

#### Credits

Mage Robes author: **Melchior Dahrk**

OpenMW-Lua script author: **johnnyhostile**

**Special Thanks**:

* **Melchior Dahrk** for making [Mage Robes](https://www.nexusmods.com/morrowind/mods/45739)
* The original author of the MWSE version <3, which I borrowed some of the "who gets what" rules from
* **The OpenMW team, including every contributor** for making OpenMW and OpenMW-CS
* **The Modding-OpenMW.com team** for being amazing
* **All the users in the `modding-openmw-dot-com` Discord channel on the OpenMW server** for their dilligent testing <3
* **Bethesda** for making Morrowind

And a big thanks to the entire OpenMW and Morrowind modding communities! I wouldn't be doing this without all of you.

#### Web

[Project Home](https://modding-openmw.gitlab.io/mage-robes-for-openmw-lua/)

<!-- [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->

[Source on GitLab](https://gitlab.com/modding-openmw/mage-robes-for-openmw-lua)

#### Installation

**OpenMW 0.49 or newer is required!**

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/mage-robes-for-openmw-lua/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\ObjectsClutter\mage-robes-for-openmw-lua

        # Linux
        /home/username/games/OpenMWMods/ObjectsClutter/mage-robes-for-openmw-lua

        # macOS
        /Users/username/games/OpenMWMods/ObjectsClutter/mage-robes-for-openmw-lua

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\ObjectsClutter\mage-robes-for-openmw-lua"`)
1. Add `content=mage-robes-for-openmw-lua.omwscripts` to your load order in `openmw.cfg` or enable them via OpenMW-Launcher

#### Lua Interface

A Lua interface is provided to allow 3rd party mods to add mod plugins and actors to this mod's internal blacklists. An "addon" mod can be created that uses the interface provided by this one to add support for the desired IDs.

To do this, two files are needed:

1. `YourAddonName.omwscripts` with the following contents:

```
GLOBAL: scripts/YourAddonName/global.lua
```

1. `scripts/YourAddonName/global.lua` with the following contents:

```lua
local MDMR = require("openmw.interfaces").MageRobes

if not MDMR then
    error("ERROR: Mage Robes for OpenMW-Lua is not installed!")
end

MDMR.AddActorToBlacklist({
        "actor_id_01",
        "actor_id_02",
        ...
})

MDMR.AddModToBlacklist({
        "CoolMod1.esp",
        "RadMod2.esp",
        ...
})
```

File layout:

```
.
├── YourAddonName.omwscripts
└── scripts
    └── YourAddonName
        └── global.lua
```

Note you should change `YourAddonName` to match your mod's name and the IDs used to match the IDs you want to add.

The `MDMR` variable gives you direct access to the Mage Robes interface. Use `MDMR.AddActorToBlacklist` to register new blacklisted actor IDs and `MDMR.AddModToBlacklist` to register blacklisted mod plugin names. You can use whatever script and path names you like, but it must be [a global script](https://openmw.readthedocs.io/en/latest/reference/lua-scripting/overview.html#format-of-omwscripts).

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/mage-robes-for-openmw-lua/-/issues)
* Email `mage-robes-for-openmw-lua@modding-openmw.com`
* Contact the author on Discord: `@johnnyhostile`
* Contact the author on Libera.chat IRC: `johnnyhostile`
