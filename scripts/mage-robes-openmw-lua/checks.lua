local core = require("openmw.core")
if core.API_REVISION < 51 then
    error(core.l10n("MDMROpenMWLua")("needNewerOpenMW"))
end
if not core.contentFiles.has("Mage Robes.ESP") then
    error(core.l10n("MDMROpenMWLua")("needMDMResp"))
end
