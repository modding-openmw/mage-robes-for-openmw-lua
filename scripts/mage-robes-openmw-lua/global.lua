require("scripts.mage-robes-openmw-lua.checks")
local world = require("openmw.world")

-- Mods to not give robes to.
local modBlacklist = {}

-- NPCs to not give robes.
local actorBlacklist = {}

-- Association between rank name and index.
local mgRank = {
    ["associate"] = 0,
    ["apprentice"] = 1,
    ["journeyman"] = 2,
    ["evoker"] = 3,
    ["conjurer"] = 4,
    ["magician"] = 5,
    ["warlock"] = 6,
    ["wizard"] = 7,
    ["masterWizard"] = 8,
    ["archMage"] = 9
}

-- A list of factions that will be checked to apply robes to.
local supportedFactions = {
    ["mages guild"] = true,
    ["t_cyr_magesguild"] = true,
    ["t_mw_magesguild"] = true,
    ["t_sky_magesguild"] = true,
    ["t_ham_magesguild"] = true
}

-- A map of magic skills, and their associated tag.
local magicSkillTags = {
    alchemy = "alc",
    alteration = "alt",
    conjuration = "con",
    destruction = "des",
    enchant = "enc",
    illusion = "ill",
    mysticism = "mys",
    restoration = "res"
}

local magicSkills = {
    alchemy = true,
    alteration = true,
    conjuration = true,
    destruction = true,
    enchant = true,
    illusion = true,
    mysticism = true,
    restoration = true
}

local robedNPCs = {}

local function highestMagicSkill(actor)
    local skill
    local val = 0
	for id, getter in pairs(actor.type.stats.skills) do
        if magicSkills[id] then
            local actorVal = getter(actor).base
            if actorVal > val then
                skill = id
                val = actorVal
            end
        end
    end
    return skill
end

local function getRobe(actor)
    -- No faction, no robe
    if actor.type.getFactions == nil then return end

    local newRobe
    for _, faction in pairs(actor.type.getFactions(actor)) do
        if supportedFactions[faction] then
            local skill = highestMagicSkill(actor)
            local rank = actor.type.getFactionRank(actor, faction)
            local tag = magicSkillTags[skill]
            if rank >= mgRank.archMage then
                newRobe = "mg_c_robe_arch"
            elseif rank >= mgRank.conjurer then
                newRobe = string.format("mg_c_robe_%smas", tag)
            elseif rank >= mgRank.apprentice then
                newRobe = string.format("mg_c_robe_%s", tag)
            else
                newRobe = "mg_c_robe_nov"
            end
            return newRobe
        end
    end
    return newRobe
end

local function deleteObj(obj)
    -- Remove the thing from its container and delete it
    obj.enabled = false
    obj:remove(1)
end

local function onActorActive(actor)
    -- Already robed
    if robedNPCs[actor.id] then return end

    -- Don't dress from a blacklisted mod
    if modBlacklist[actor.contentFile] then
        robedNPCs[actor.id] = true
        return
    end

    -- Don't dress a blacklisted actor
    if actorBlacklist[actor.recordId] then
        robedNPCs[actor.id] = true
        return
    end

    -- Get equipment
    local robeSlot = actor.type.EQUIPMENT_SLOT.Robe
    local equipped = actor.type.equipment(actor)

    -- Get the currently equipped robe
    local equippedRobe = equipped[robeSlot]

    -- Don't put a robe on an NPC that doesn't already have one
    if equippedRobe == nil then
        robedNPCs[actor.id] = true
        return
    end

    -- Get the currently equipped robe record for data access
    local equippedRec = equippedRobe.type.record(equippedRobe)

    -- Don't swap out enchanted clothing
    if equippedRec.enchant then
        robedNPCs[actor.id] = true
        return
    end

    -- Don't swap out scripted clothing
    if equippedRec.mwscript then
        robedNPCs[actor.id] = true
        return
    end

    -- Try to get the appropriate robe for this actor
    local robeId = getRobe(actor)
    if not robeId then
        robedNPCs[actor.id] = true
        return
    end

    -- Create the robe...
    local newRobe = world.createObject(robeId)

    -- ... but don't swap out more valuable clothing
    if equippedRec.value > newRobe.type.record(newRobe).value then
        robedNPCs[actor.id] = true
        deleteObj(newRobe)
        return
    end

    -- Okay, this NPC gets a robe!
    newRobe:moveInto(actor.type.inventory(actor))
    actor:sendEvent("momw_mdmr_equipRobe", newRobe)
    robedNPCs[actor.id] = true

    -- Send the old robe to Oblivion
    deleteObj(equippedRobe)
end

local function onLoad(data)
    robedNPCs = data.robedNPCs
end

local function onSave()
    return {robedNPCs = robedNPCs}
end

local function addModToBlacklist(pluginNames)
    for _, name in pairs(pluginNames) do
        modBlacklist[string.lower(name)] = true
    end
end

local function addActorToBlacklist(actorIds)
    for _, id in pairs(actorIds) do
        actorBlacklist[string.lower(id)] = true
    end
end

-- Default blacklistings
addModToBlacklist({
        "morrowind.esm",
        "bloodmoon.esm",
        "tribunal.esm",
        "Astrologians Guild.esp"
})
addActorToBlacklist({
    "Sky_qRe_KWMG_JiTavarad",
    "TR_m3_Arquebald Vene"
})

return {
    engineHandlers = {
        onActorActive = onActorActive,
        onLoad = onLoad,
        onSave = onSave
    },
    interfaceName = "MageRobes",
    interface = {
        AddActorToBlacklist = addActorToBlacklist,
        AddModToBlacklist = addModToBlacklist
    }
}
