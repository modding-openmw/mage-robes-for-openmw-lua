require("scripts.mage-robes-openmw-lua.checks")
local self = require("openmw.self")

local function equipRobe(robeObj)
    -- Equip the new, fancy robe
    local equipped = self.type.equipment(self)
    equipped[self.type.EQUIPMENT_SLOT.Robe] = robeObj.recordId
    self.type.setEquipment(self, equipped)
end

return {eventHandlers = {momw_mdmr_equipRobe = equipRobe}}
